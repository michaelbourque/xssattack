// JavaScript Document


// INFR-3120 Assignment #1
// October 22, 2013
// Michael Bourque - 100258740


//A function to return a random integer between 1 and 1000
function getRandomInt()
{
	var x=Math.floor((Math.random()*1000)+1);
	return x;
}


//The main function
$(function(){
	
	var f=$('iframe'); //Shortening the most fequently accessed object
	
	f.load(function(){ //When the iFrame loads, do the following:
		
			f.contents().find("body").append("<div id='message4'></div>");  // Preload Message #3
			f.contents().find("#message4").html("You in 'da Matrix now son!");  // Stuff content into Message #4
			f.contents().find("#message4").css({"font-size":"40px","height":"160px","width":"400px","margin":"0 auto","border":"3px solid #0F0","top":"1200px",
											   				"position":"absolute", "text-align":"center", "padding-top":"5px", "background-color":"#333", "border-radius":"15px",
											  				"box-shadow":"5px 5px 5px #666", "left":"450px","color":"#FFF","font-family":"courier,sans-serif","letter-spacing":"5px",
											   				"text-transform":"uppercase","line-height":"40px", "opacity":"0.75"});  // Add styles to Message #3	
			f.contents().find("body").append("<div id='message3'></div>");  // Preload Message #3
			f.contents().find("#message3").html("What's happening to your site hombre?");  // Stuff content into Message #3
			f.contents().find("#message3").css({"font-size":"40px","height":"160px","width":"400px","margin":"0 auto","border":"3px solid #0F0","top":"1200px",
											   				"position":"absolute", "text-align":"center", "padding-top":"5px", "background-color":"#333", "border-radius":"15px",
											  				"box-shadow":"5px 5px 5px #666", "left":"450px","color":"#FFF","font-family":"courier,sans-serif","letter-spacing":"5px",
											   				"text-transform":"uppercase","line-height":"40px", "opacity":"0.75"});  // Add styles to Message #3	
			f.contents().find("body").append("<div id='message2'></div>");  // Preload Message #2
			f.contents().find("#message2").html("Hmmm, that can't be good dude!");  // Stuff content into Message #2
			f.contents().find("#message2").css({"font-size":"40px","height":"100px","width":"500px","margin":"0 auto","border":"3px solid #0F0","top":"1200px",
											   "position":"absolute", "text-align":"center", "padding-top":"5px", "background-color":"#333", "border-radius":"15px",
											   "box-shadow":"5px 5px 5px #666", "left":"425px","color":"#0F0","font-family":"courier,sans-serif","letter-spacing":"5px",
											   "text-transform":"uppercase","line-height":"40px", "opacity":"0.75"});  // Add styles to Message #2
			f.contents().find("body").append("<div id='message1'></div>").each(   // Load Message #1 and provide a callback function
			
			
			//Call back function for append//message1
			function() {
 
				f.contents().find("#message1").html("<p>Uh oh! Somethings wrong Bud!</p>"); //Stuff content into Message #1
				f.contents().find("#message1").css({"font-size":"40px","height":"90px","width":"500px","margin":"0 auto","border":"3px solid #0F0","top":"400px",
											   "position":"absolute", "text-align":"center", "padding-top":"5px", "background-color":"#333", "border-radius":"15px",
											   "box-shadow":"5px 5px 5px #666", "left":"425px","color":"#0F0","font-family":"courier,sans-serif","letter-spacing":"5px",
											   "text-transform":"uppercase", "line-height":"40px", "opacity":"0.75"}).each(  //Add styles to Message #1 and provide a callback function
											   
				//Call back function for css//message1
				function() {
		
			
					//Shuffle Messages
					f.contents().find('#message1').animate({top:-150}, 2000);  //Transition Message #1 out
					f.contents().find('#message2').delay(500).animate({top:300},3000);    //Transition Message #2 in
					
					//Commence Animation to dismantle website
					f.contents().find('a').animate({backgroundPositionY:100}, 3000, "swing", //Remove background images to hyper links (slide down)
					
					//Call back function for animate//a (links)
					function() {
						
						// Apply Matrixy CSS to p tags
						f.contents().find('p').css({"color":"#0F0", "font-family":"courier,sans-serif", "letter-spacing":"5px", "text-transform":"uppercase"});
						// Apply bold CSS to li tags
						f.contents().find('li').css({"color":"#0F0", "font-family":"courier,sans-serif", "letter-spacing":"5px", "text-transform":"uppercase", "font-weight":"bold"});

					});	// End animate//a function 


	
				//Continue Dismantling Website		
			
				f.contents().find('#website_title').css({"position":"relative"}); //Make div independently movable
				f.contents().find('#templatemo_banner').css({"position":"relative"}); //Make div independently movable		
	    		f.contents().find('#templatemo_header').animate({backgroundPositionY:120},1000, "swing") // Collapse Header
				f.contents().find('#templatemo_banner').delay(850).animate({backgroundPositionY:280},1000, "linear") // Collapse Banner (after delay to sync with header)
			
				//Shuffle Messages
				f.contents().find('#message2').animate({top:-150}, 2000, "swing");
				f.contents().find('#message3').delay(850).animate({top:280},5000);    //Transition Message #3 in
			
				//Sequentially disasemble remaining HTML elements with animation (sliding out and collapsing)
				f.contents().find('#website_title').delay(2500).animate({left:1000}, 3000, "swing");
				f.contents().find('#templatemo_menu').css({"position":"relative"});			
				f.contents().find('#templatemo_menu').delay(2500).animate({left:-1000}, 3000, "swing");
				f.contents().find('#templatemo_menu').css({"position":"fixed"});
				f.contents().find('#templatemo_content').delay(2500).animate({top:-400}, 3000, "swing");
				f.contents().find("img").delay(4500).animate({width:0}, 3000, "swing");
				f.contents().find("img").delay(4500).animate({height:0}, 3000, "swing");
				f.contents().find("input").delay(4500).animate({width:0}, 1000, "swing");
				f.contents().find("input").animate({height:0}, 1000, "swing");
				f.contents().find("input").animate({margin:0, padding:0, borderWidth:0}, 1000, "swing");
				f.contents().find("select").delay(4500).animate({width:0}, 3000, "swing");
				f.contents().find("select").delay(4500).animate({height:0}, 3000, "swing");	
			
				//Fly away remaining elements
				f.contents().find(".content_title_01").css({"position":"relative"});
				f.contents().find(".content_title_01").delay(6500).animate({left:-1200}, 1500, "swing");
				f.contents().find("label").css({"position":"relative"});
				f.contents().find("label").delay(7500).animate({left:1200}, 1000, "swing");
				f.contents().find(".news_title").css({"position":"relative"});
				f.contents().find(".news_title").delay(8000).animate({left:1200}, 1000, "swing");
				f.contents().find(".content_left_section").find("p").css({"position":"relative"});
				f.contents().find(".content_left_section").find("p").delay(8200).animate({left:1200}, 800, "swing");
				f.contents().find(".content_right_section").find("p").css({"position":"relative"});
				f.contents().find(".content_right_section").find("p").delay(8200).animate({left:-1200}, 800, "swing");
				f.contents().find(".content_right_2column_box").find("p").css({"position":"relative"});
				f.contents().find(".content_right_2column_box").find("p").delay(8200).animate({left:-1200}, 800, "swing");
				f.contents().find(".content_right_2column_box").find("li").css({"position":"relative"});
				f.contents().find(".content_right_2column_box").find("li").delay(8200).animate({left:-1200, top:0}, 500, "swing");
				f.contents().find("a").delay(5000).fadeOut();
				f.contents().find("#templatemo_container").delay(8500).animate({width:0, height:0, top:0, right:0}, 1000, "swing");
				f.contents().find('#message2').animate({top:-200}, 2000);  // remove message #2
				
				//Fade Out all remaining elements
		    	f.contents().find("body").find("div").find("*").delay(9000).fadeOut(); 
			
				//Hacked Message
				f.contents().find('#message3').animate({top:-300}, 1000, 
				function() {
					f.contents().find('#message4').animate({top:200}, 500);
					f.contents().find('#message4').animate({opacity:0}, 2000);
				});
				
				f.contents().find("body").append("<div id='message5'></div>").each( //Introduce message #4 with callback
				
			
				//Callback function for append//message5
				function() {
				
			    	f.contents().find("body").append("<div id='click'></div>"); //add click message
					
					f.contents().find("#click").css({"font-size":"14px","color":"#CCC","text-align":"center","top":"150px", "position":"relative"}) //style the click button
					f.contents().find("#message5").append("Dude!You got h@ck3d"); //stuff Message #4
					f.contents().find("#message5").css({"font-size":"40px","height":"50px","width":"400px","margin":"0 auto","border":"3px solid #0F0","top":"300px",
												   		"position":"relative", "text-align":"center", "padding-top":"30px", "background-color":"#333", "border-radius":"15px",
														"box-shadow":"5px 5px 5px #666", "opacity":"0.75"});  // Apply styles to message #4
					
					// Clickable button } click here {
					f.contents().find("#click").append("} click here {").click( //create a click.event listener
			
					
					//onClick event
					function() { 	
										
						f.contents().find("body").find("div").fadeOut();
				
						f.contents().find("#click").remove();  // remove the click button
						f.contents().find("#message5").animate({top:-200}, 1500);
											
				        //Append CSS Styles for new site
				        f.contents().find("head").append("<link href='http://www.adflavor.net/form_02.css' rel='stylesheet' type='text/css'>");
				        f.contents().find("head").append("<link href='http://www.adflavor.net/global.css' rel='stylesheet' type='text/css'>");
										
						//Reset some modified CSS properties
						f.contents().find("body").css({"text-align":"center","background":"#311a03","color":"#888","font-family":"Arial, Helvetica, sans-serif","text-transform":"none","letter-spacing":"normal"});
    						
						//Phase in new page with transitions
						f.contents().find("body").append("<div id='headerWrap'></div>");
						f.contents().find("#headerWrap").append("<div id='navWrap'></div>");
						f.contents().find("#headerWrap").css({"margin-left":"1200px"});
						f.contents().find("#headerWrap").animate({marginLeft:0}, 3000).each(  //slideIn header with callback
					    	
						//Callback function for animate//headerWrap
						function() {
						
							//Build Header
							f.contents().find("#navWrap").append("<div id='logo'></div>");
							f.contents().find("#logo").append("<a href='#'></a>");
							f.contents().find("#logo").find("a").append("<img src='http://www.adflavor.net/img/logo_header.png'></img>");
							f.contents().find("#navWrap").append("<div id='menu'></div>");
							f.contents().find("#menu").append("<div class='menuitems'><a href='#' class='rollover5'></a></div>");
							f.contents().find("#menu").append("<div class='menuitems'><a href='#' class='rollover4'></a></div>");
							f.contents().find("#menu").append("<div class='menuitems'><a href='#' class='rollover3'></a></div>");	
							f.contents().find("#menu").append("<div class='menuitems'><a href='#' class='rollover2'></a></div>");
							f.contents().find("#menu").append("<div class='menuitems'><a href='#' class='rollover1'></a></div>");	
								
						}); // End function for animate//headerwrap
							
						//Build Banner
						f.contents().find("#headerWrap").append("<div id='pusherWrap'></div>");
						f.contents().find("#pusherWrap").append("<div id='pusher'></div>");
						f.contents().find("#pusherWrap").css({"opacity":"0"});
						f.contents().find("#pusher").append("<script language='JavaScript'>//showImage();</script><img src='http://www.adflavor.net/img/pushers/pusher_berry.png'></div>'");
						f.contents().find("#pusherWrap").append("<div id='pusherText'></div>");
						f.contents().find("#pusherText").append("<h1><img src='http://www.adflavor.net/img/it_justAdFlavor.gif' alt='Just AdFlavor'></h1>");
						f.contents().find("#pusherText").append("<p>With AdFlavors effective advertising network, we excel in creating high quality Advertisers and powerful Publishers. AdFlavor's zero financial risk and high revenue potential, we make Advertisers and Publishers alike extremely effective at exceeding their target needs. Instead of being fluffed up with fancy words, find out for yourself how we can exceed your advertising needs today.</p>");
						f.contents().find("#pusherText").append("<p><a href='#'><img src='http://www.adflavor.net/img/btn_readMoreDarkMatte.gif' alt='Read More'></a></p>");
							
						//Build Content
						f.contents().find("body").append("<div id='contentWrap'></div>").each( //add Content Wrapper with callback
							
						//Callback function for appaend//contentWrap
						function() {  
						
							f.contents().find("#contentWrap").css({"margin-left":"-1200px"});
							f.contents().find("#contentWrap").animate({marginLeft:0}, 3000, 
							
							//Callback function for animate//contentWrap
							function() {
								
								f.contents().find("#pusherWrap").animate({opacity:1}, 3000); 
							
							});  // End function animate//contentWrap
						
						});  //End function append//contentWrap
							
						//Continue Building Content
						f.contents().find("#contentWrap").append("<div id='content'></div>");
						f.contents().find("#content").append("<div class='col320Fixed'><h1><img src='http://www.adflavor.net/img/it_advertisers_whyAdflavor.gif' alt='Advertising Why Just AdFlavor'></h1><img src='http://www.adflavor.net/img/i_graph.png' alt='Performance Based Advertising Network'><p>AdFlavor is a performance-based online advertising network. Advertisers can pick their desired metric and will only pay when an action occurs. Our goal is to work within our clients' target objectives to help achieve the highest ROI for each and every campaign.</p></div>");
						f.contents().find("#content").append("<div class='col320Fixed'><h1><img src='http://www.adflavor.net/img/it_publishers_continualExcellence.gif' alt='Publishers Continual Excellence'></h1><img src='http://www.adflavor.net/img/i_speedometer.png' alt='Performance Test'><p>AdFlavor tests every promotion internally to maximize response. Only true \"winners\" are placed in the Adflavor network and even then, we continually optimize to ensure maximum performance. Publishers have access to a wide selection of profitable offers from high-end Advertisers to drive the highest payout results.</p></div>");
						f.contents().find("#content").append("<div class='col220'><h1><img src='http://www.adflavor.net/img/it_clientLogin.gif' alt='Client Login'></h1><form name='aspnetFormLogin' method='post' action='http://affiliate.adflavor.net/welcome/customlogin.aspx' id='aspnetFormLogin'><fieldset><label class='usernameLogin'>Username<input type='text/' name='UserName' id='ctl00_ContentPlaceHolder1_txtUserName'></label><label class='pwdLogin'>Password<input type='password' name='Password' id='ctl00_ContentPlaceHolder1_txtPassword'></label><input name='ctl00$ContentPlaceHolder1$btnLogin' id='ctl00_ContentPlaceHolder1_btnLogin' method='post' action='http://affiliate.adflavor.net/welcome/customlogin.aspx' type='image' value='Login' class='submitLogin' src='http://www.adflavor.net/img/btn_login.gif' alt='Submit Application'></fieldset></form><p><a href='#'>Forgot your password?</a></p><h2><a href='#'><img src='http://www.adflavor.net/img/btn_joinAdFlavor.gif' alt='Join AdFlavor Today'></a></h2></div>");
						f.contents().find("#content").append("<div class='clear'></div>");
						f.contents().find("#content").append("<div class='col320Fixed'><a href='#'><img src='http://www.adflavor.net/img/btn_findOutMore_lightMatte.gif' alt='Find Out More'></a></div>");
						f.contents().find("#content").append("<div class='col320Fixed'><a href='#'><img src='http://www.adflavor.net/img/btn_findOutMore_lightMatte.gif' alt='Find Out More'></a></div>");
						
						//Build Footer
						f.contents().find("body").append("<div id='footerWrap'></div>");
						f.contents().find("#footerWrap").append("<div id='footer'></div>");
						f.contents().find("#footer").append("<div id='footerLogo'></div>");
						f.contents().find("#footerLogo").append("<h1><a href='#'><img src='http://www.adflavor.net/img/logo_adflavor_footer.gif' alt='AdFlavor'></a></h1><p>© 2009 AdFlavor. All rights reserved.</p><p><a href='#' target='top'>Privacy Policy</a></p>");
						f.contents().find("#footer").append("<div id='footerMenu'></div>");
						f.contents().find("#footerMenu").append("<h1><a href='#'>Home</a></h1><ul><li><a href='#'>Advertisers</a></li><li><a href='#'>Publishers</a></li><li><a href='#'>About AdFlavor</a></li><li><a href='#'>Client List</a></li><li><a href='#'>Contact Us</a></li></ul>");
						f.contents().find("#footer").append("<div id='social'></div>");
						f.contents().find("#social").append("<h1>Follow us around</h1><ul><li><a href='#'><img src='http://www.adflavor.net/img/icon_twitter.gif' alt='Twitter'></a></li><li><a href='#'><img src='http://www.adflavor.net/img/icon_facebook.gif' alt='Facebook'></a></li><li><a href='#'><img src='http://www.adflavor.net/img/icon_rss.gif' alt='RSS'></a></li></ul>");
						f.contents().find("#footer").append("<div id='featuredClients'></div>");
						f.contents().find("#featuredClients").append("<ul><li></li><li><img src='http://www.adflavor.net/img/clients/dish_sm.png' alt='Dish Network'></li><li><img src='http://www.adflavor.net/img/clients/hsbc_sm.png' alt='HSBC'></li><li><img src='http://www.adflavor.net/img/clients/sears_sm.png' alt='Sears'></li><li><img src='http://www.adflavor.net/img/clients/verizon_sm.png' alt='Verizon'></li></ul>");
						
						f.contents().find("body").append("<div id='message6'></div>");
						f.contents().find("body").find("#message6").fadeIn();
						f.contents().find("#message6").css({"font-size":"40px","height":"120px","width":"400px","margin":"0 auto","border":"3px solid #0F0","top":"250px",
									   						"position":"absolute", "text-align":"center", "padding-top":"5px", "background-color":"#333", "border-radius":"15px",
															"box-shadow":"5px 5px 5px #666", "left":"425px","color":"#0F0","font-family":"courier,sans-serif","letter-spacing":"5px",
															"text-transform":"uppercase", "line-height":"40px","opacity":"0"});
						f.contents().find("#message6").html("Your $#!% just got jacked!");
						f.contents().find("#message6").animate({opacity:0.75}, 5000);
							
								
					}); //End function for click.event
	
				}); //End function for append//message5
	
	 		}); //End function for css//message1

		}); //End function for append//message1

	
	}); // Load function
	
}) // Main Function
